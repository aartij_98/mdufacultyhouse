from django.db import models
from django.utils.datetime_safe import date as currentDate
# Create your models here.

class Room(models.Model):
    """Class Modelling Rooms """
    # number = models.IntegerField()
    number = models.CharField(max_length=32)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Room No: " + str(self.number)


class Status(models.Model):
    room = models.ManyToManyField(Room)
    # date = models.DateField()
    # name = room.number
    booked_for = models.CharField(max_length=256, blank=True)
    checkin = models.DateField()
    checkout = models.DateField()
    updated_on = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=15, choices=(
        ('available', 'Available'),
        ('booked', 'Booked'),
        ('inactive', 'Reserved')
    ))
    class Meta:
        ordering = ["updated_on"]

    def __str__(self):
        if self.status == 'available':
            status = 'Available'
        elif self.status == 'booked':
            status = 'Booked'
        else:
            status = 'Reserved'
        rooms = ''
        for obj in self.room.all():
            print(obj)
            rooms = rooms + ' '+ str(obj.number)
        total_rooms = len(self.room.all())
        return 'Rooms: '+rooms + '\n ('+ status + " From " + str(self.checkin) +' To ' \
               + str(self.checkout) + ' )' + '\n[Total Rooms: '+str(total_rooms) + ']' \
               + '\nBooked For ' +str(self.booked_for).capitalize()


# class Booking(models.Model):
#     room = models.ForeignKey(Room, on_delete=models.CASCADE)
#     booked_from = models.DateField()
#     booked_till = models.DateField()

    # def __str__(self):
    #     return "Room No: " + str(self.room.number) + ' Booked From: '+str(self.booked_from) + ' Booked Till: '+ str(self.booked_till)