from django.shortcuts import render
from .models import Room,Status
from django.db.models import F,Q
from datetime import date,datetime
from .forms import Get_date
# Create your views here.


def index(request):
    if request.method == 'GET':
        form = Get_date(request.GET)
        context = {}
        context['form'] = form
        test_date = str(date.today())
        if form.is_valid():
            test_date = form.cleaned_data['test_date']
        booked_rooms = {}
        reserved_rooms = {}
        rooms = Room.objects.all()
        for room in rooms:
            # booked_rooms[room.number] = Status.objects.filter(room__number = room.number, status='booked', date=str(date.today()))
            booked_rooms[room.number] = Status.objects.distinct().filter(room__number = room.number, status='booked', checkin__lte=test_date, checkout__gte=test_date)
            reserved_rooms[room.number] = Status.objects.distinct().filter(room__number=room.number, status='inactive',checkin__lte=test_date, checkout__gte=test_date)

        last_updated = (Status.objects.order_by('-updated_on')[0]).updated_on
        context['last_updated'] = last_updated

        context['booked_rooms'] = booked_rooms
        context['reserved_rooms'] = reserved_rooms

    # context = {}
    # # objs = Status.objects.filter(date=dateToday).order_by('-updated_on')
    # objs = Status.objects.all()
    # # rooms = [obj.room.number for obj in objs]
    # # status = [obj.status for obj in objs]
    # last_updated = (Status.objects.order_by('-updated_on')[0]).updated_on
    # # room_status = dict(zip(rooms,status))
    # # context['room_status'] = room_status
    # context['last_updated'] = last_updated
    # context['objs'] = objs
    # print(context)
    #
    return render(request,'index.html',{'context':context})

def index_test(request):
    if request.method == 'GET':
        form = Get_date(request.GET)
        context = {}
        context['form'] = form
        test_date = form.cleaned_data['test_date']
        booked_rooms = {}
        reserved_rooms = {}
        rooms = Room.objects.all()
        for room in rooms:
            booked_rooms[room.number] = Status.objects.filter(room__number = room.number, status='booked', date=str(date.today()))
            reserved_rooms = Status.objects.filter(room__number=room.number, status='inactive',date=str(date.today()))
        context['booked_rooms'] = booked_rooms
        context['reserved_rooms'] = reserved_rooms
    return render(request, 'test.html', {'context': context})


def status(request):
    if request.method == 'GET' :
        form = Get_date(request.GET)
        test_date = str(date.today())
        if form.is_valid():
            test_date = form.cleaned_data['test_date']

        context = {}
        booked_rooms = {}
        reserved_rooms = {}
        rooms = Room.objects.all()
        print("Executing")
        print(test_date)
        test_date = datetime.strptime(test_date, '%Y-%m-%d')
        print(test_date)
        for room in rooms:
            booked_rooms[room.number] = Status.objects.filter(room__number = room.number, date=test_date)#, status='booked')
            reserved_rooms = Status.objects.filter(room__number=room.number, status='inactive',date=str(date.today()))
        context['booked_rooms'] = booked_rooms
        context['reserved_rooms'] = reserved_rooms
        context['form'] = form
    # context = {}
    # # objs = Status.objects.filter(date=dateToday).order_by('-updated_on')
    # objs = Status.objects.all()
    # # rooms = [obj.room.number for obj in objs]
    # # status = [obj.status for obj in objs]
    # last_updated = (Status.objects.order_by('-updated_on')[0]).updated_on
    # # room_status = dict(zip(rooms,status))
    # # context['room_status'] = room_status
    # context['last_updated'] = last_updated
    # context['objs'] = objs
    # print(context)
    #
    return render(request,'index.html',{'context':context})


def test(request, test_date):
    objs = Status.objects.filter()
    rooms = [obj.room.number for obj in objs]
    status = [obj.status for obj in objs]
    last_udpated = (Status.objects.order_by('-updated_on')[0]).updated_on

    context = dict(zip(rooms,status))
    context['last_updated'] = last_udpated
    # print(context)
    return render(request,'test.html',{'data':context})